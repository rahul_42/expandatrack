<?php

namespace ExpandatrckBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use ExpandatrckBundle\Entity\Variation;
use Symfony\Component\Filesystem\Filesystem;
class VariationsController extends Controller {

    /**
     * @Route("/service/{id}/variations",name="variations")
     * @Template()
     */
    public function indexAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        
        $service = $em->getRepository('ExpandatrckBundle:Service')->find($id);

        if (!$service) {
            throw $this->createNotFoundException('Unable to find Service entity.');
        } 
        $variation = $service->getVariation();
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $variation, $request->query->getInt('page', ($request->get('page')) ? $request->get('page') : 1)/* page number */, 10/* limit per page */
        );
        $allservice = $em->getRepository('ExpandatrckBundle:Service')->findAll();
        return array(
            'entities' => $pagination,
            'page_title' => 'Variations',
            'service_id' => $id,
            'services'=>$allservice,
            'pagination' => $pagination,
            'servicename'=>$service->getServiceName()
        );
    }

    /**
     * @Route("/add/variations/",name="add_variations")
     * 
     * @Template()
     */
    public function addAction(Request $request) {
        
        $em = $this->getDoctrine()->getManager();
        $entity = new Variation();
        $form_data = $request->request->all();
        $serviceid = $form_data['serviceid'];

        $service = $em->getRepository('ExpandatrckBundle:Service')->find($serviceid);

        if (!$service) {
            throw $this->createNotFoundException('Unable to find Service entity.');
        }
        $file = $request->files->get('variation_image');
        
        if (!empty($file)) {
           
            $fs = new Filesystem();
            try {
                
                if (!file_exists("uploads/variation/")) {
                     
                    $fs->mkdir("uploads/variation");
                    
                }
            } catch (IOExceptionInterface $e) {
                echo "An error occurred while creating your directory at " . $e->getPath();
            }
            $filename = time() . $file->getClientOriginalName();
            $file->move("uploads/variation/", $filename);
            $entity->setPath($filename);
        }
        $entity->setStartHeight($form_data['start_height']);
        $entity->setEndHeight($form_data['end_height']);
        $entity->setStartWith($form_data['start_with']);
        $entity->setEndWith($form_data['end_with']);
        $entity->setCost($form_data['cost']);
        $entity->setAlterText($form_data['alter']);
        $entity->setService($service);

        $em->persist($entity);
        $em->flush();
        return $this->redirect($this->generateUrl('variations', array('id' => $serviceid)));
    }
    
    /**
     * @Route("/edit/variations/",name="edit_variations")
     * 
     * @Template()
     */
    public function editAction(Request $request) {
         $form_data = $request->request->all();
         $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Variation')->find($form_data['variationid']);
        $page_number = ($form_data['pagenumber']) ? $form_data['pagenumber'] : 1; 
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Variation entity.');
        } 
        
        $file = $request->files->get('variation_image');
        
        if (!empty($file)) {
           
            $fs = new Filesystem();
            try {
                
                if (!file_exists("uploads/variation/")) {
                     
                    $fs->mkdir("uploads/variation");
                    
                }
            } catch (IOExceptionInterface $e) {
                echo "An error occurred while creating your directory at " . $e->getPath();
            }
            $filename = time() . $file->getClientOriginalName();
            $file->move("uploads/variation/", $filename);
            $entity->setPath($filename);
        }
        
        $entity->setStartHeight($form_data['start_height']);
        $entity->setEndHeight($form_data['end_height']);
        $entity->setStartWith($form_data['start_with']);
        $entity->setEndWith($form_data['end_with']);
        $entity->setCost($form_data['cost']);
        $entity->setAlterText($form_data['alter']);
        $em->persist($entity);
        $em->flush();
        return $this->redirect($this->generateUrl('variations',array('id'=>$form_data['serviceid'],'page'=>$page_number)));
    }
    
    /**
     * @Route("/delete/variations/{id}/{serviceid}",name="delete_variations")
     * @Template()
     */
    public function deleteAction(Request $request, $id,$serviceid) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Variation')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Variation entity.');
        }
       try {
            $em->remove($entity);
            $em->flush();
        } catch (\Exception $e) {
            $request->getSession()
          ->getFlashBag()
          ->add('error', 'Installation can not delete!')
         ;
       }
        return $this->redirect($this->generateUrl('variations',array('id'=>$serviceid)));
    }

}
